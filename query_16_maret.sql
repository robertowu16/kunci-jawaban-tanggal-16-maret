--1.tampilkan data karyawan yang merupakan Sales dengan jabatan Branch Manager 
--dan sudah bekerja lebih dari 10 tahun
select 
m.Name,
mt.name,
mpt.Name,
m.EmployedAt
from tb_m_Manpower m 
join tb_m_ManpowerType mt on m.ManpowerTypeId = mt.ManpowerTypeId
join tb_m_ManpowerPositionType mpt on m.LastManpowerPositionTypeId = mpt.ManpowerPositionTypeId
where mt.Name = 'Sales' and mpt.Name = 'Branch Manager' and datediff(year,m.EmployedAt,getdate())>10

--2.Tampilkan 10 data karyawan yang paling baru bekerja di "Astrido Toyota Pondok Cabe", 
--dengan kondisi karyawan tersebut belum resign
select top 10 
m.Name,
o.Name,
m.EmployedAt
From tb_m_Manpower m
join tb_m_Outlet o on m.outletid = o.OutletId
where o.Name ='Astrido Toyota Pondok Cabe' and m.ResignedAt is null order by EmployedAt desc

--3.Tampilkan data karyawan yang bekerja untuk outlet yang status kepemilikan gedungnya masih sewa
select 
m.Name,
ssp.Name,
sspbos.Name
from tb_m_Manpower m
join tb_m_SalesServicePoint ssp on m.SalesServicePointId = ssp.SalesServicePointId
join tb_m_SalesServicePointBuildingOwnershipStatus sspbos 
on ssp.SalesServicePointBuildingOwnershipStatusId = sspbos.SalesServicePointBuildingOwnershipStatusId
where sspbos.Name = 'Rent'

--4.Tampilkan data kompetitor untuk outlet "Nasmoco Bantul".
--by function
select 
o.Name,cl.CompetitorCompanyName
from tb_m_CompetitorLocation cl
join tb_m_Competitor c on cl.CompetitorId = c.CompetitorId
join tb_m_Outlet o on cl.OutletFunctionId = o.OutletFunctionId
where o.Name = 'Nasmoco Bantul'

--by location
select 
o.Name,cl.CompetitorCompanyName
from tb_m_CompetitorLocation cl
join tb_m_Competitor c on cl.CompetitorId = c.CompetitorId
join tb_m_Outlet o on cl.KabupatenId = o.KabupatenId
where o.Name = 'Nasmoco Bantul'

--5.Tampilkan data outlet milik AUTO2000 yang diresmikan diatas tahun 2000
select
	o.Name,
	o.AuthorizeDate,
	dg.DealerBrandName
from tb_m_Outlet o 
join tb_m_DealerCompany dc on o.DealerCompanyId = dc.DealerCompanyId
join tb_m_DealerGroup dg on dc.DealerGroupId = dg.DealerGroupId
where dg.DealerBrandName = 'AUTO2000' and o.AuthorizeDate > '2000'

--6.Tampilkan data total penjualan mobil Yaris dan Agya diatas tahun 2010 berdasarkan jenis mobilnya
select 
cm.Name,sum(Quantity) as [Total Quantity]
from tb_r_RetailSales rs
join tb_m_CarModel cm on rs.CarModelId = cm.CarModelId
where cm.name in ('agya','yaris') and SalesYear > 2010
group by cm.Name


--7.Tampilkan data total penjualan mobil hatchback pada tahun 2015
select
ct.name,
sum(quantity) as[penjualan hatchback]
from tb_r_RetailSales rs
join tb_m_CarModel cm on rs.CarModelId = cm.CarModelId
join tb_m_CarType ct on cm.CarTypeId = ct.CarTypeId
where ct.Name = 'hatchback' and rs.SalesYear = 2015
group by ct.Name